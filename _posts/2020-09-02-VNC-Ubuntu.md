---
layout: post
title:  "Configurar VNC en Ubuntu, incluyendo login"
date:   2020-09-02 13:30:00 +0100
categories: ubuntu vnc lightdm gdm3
---

En [StackOverflow](https://askubuntu.com/questions/1033274/ubuntu-18-04-connect-to-login-screen-over-vnc) explican cómo configurar un ordenador con Ubuntu para acceder por VNC desde la pantalla de login.

## Instalar LightDM
GDM3 es el _display manager_ por defecto desde hace bastante, pero no tiene soporte para VNC. Por eso instalamos LightDM.

`apt install lightdm`

Si fuera necesario restaurar GDM3:
`dpkg-reconfigure gdm3`

## Instalar x11vnc
`apt install x11vnc`

## Crear servicio de systemd
Creamos el archivo de configuración del servicio en `/etc/systemd/system/x11vnc.service` (con permisos de superusuario)

{% highlight INI %}
# Description: Custom Service Unit file
# File: /etc/systemd/system/x11vnc.service
[Unit]
Description="x11vnc"
Requires=display-manager.service
After=display-manager.service

[Service]
ExecStart=/usr/bin/x11vnc -loop -nopw -xkb -repeat -noxrecord -noxfixes -noxdamage -forever -rfbport 5900 -display :0 -auth guess
ExecStop=/usr/bin/killall x11vnc
Restart=on-failure
RestartSec=2

[Install]
WantedBy=multi-user.target
{% endhighlight %}

## Activar servicio
`systemctl enable x11vnc.service`

## Notas
Todo esto se hace suponiendo que :0 representa el monitor de X que se va a usar, y x11vnc trabaja en ese monitor. Se podría crear un monitor virtual y que x11vnc trabaje en ese monitor.

- En caso de usar virt-manager (que también activa VNC en 127.0.0.1:5900) habría que indicar a x11vnc que escuche en la IP de la LAN (-listen 192.168.X.X)
- Los parámetros usados para iniciar x11vnc indican que no hace falta contraseña (-nopw)

## Cuestiones que me planteo
- ¿Qué pasaría si LightDM dejara de tener soporte?
- ¿Tendrá GDM3 soporte para VNC en algún momento?
